import random


def hello_world():
    print("hello world!")


def hello_me():
    name = input("Name?")
    print("Hello " + name)


def hello_language():
    name = input("Name?")
    language = input("Language?")

    if language == "swedish":
        greeting = "Hej"
    elif language == "spanish":
        greeting = "Hola"
    else:
        greeting = "Hello"

    print(greeting + " " + name)


def hello_language_dict():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    language = input("Language?")
    print(greetings_dict.get(language) + " " + name)


def hello_in_line_if():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    input_var = input("Language?")
    language = input_var if input_var is not '' else 'english'

    print(greetings_dict.get(language) + " " + name)


def hello_formatted_string():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    input_var = input("Languge?")
    language = input_var if input_var is not '' else 'english'

    print('%s %s' % (greetings_dict.get(language), name))

def hello_random_lang():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    input_var = input("Languge?")
    language = input_var if input_var is not '' else random.choice(list(greetings_dict.keys()))

    print('%s %s' % (greetings_dict.get(language), name))


#-------------------------- Implement the algorithm