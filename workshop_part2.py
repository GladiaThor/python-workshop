import random
import sys


def hello_random_loop():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    input_var = input("Languge?")
    language = input_var if input_var is not '' else random.choice(list(greetings_dict.keys()))

    max_iterations = random.randint(1, 10)
    current_iteration = 0
    while current_iteration < max_iterations:
        print('%s %s' % (greetings_dict.get(language), name) )
        current_iteration += 1



def hello_options_ugly():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")
    keys_list = greetings_dict.keys()

    input_var = input("Languge? opntions: %s" %keys_list)
    language = input_var if input_var is not '' else random.choice(list(greetings_dict.keys()))

    max_iterations = random.randint(1, 10)
    current_iteration = 0
    while current_iteration < max_iterations:
        print('%s %s' % (greetings_dict.get(language), name) )
        current_iteration += 1


def hello_options_pretty():
    greetings_dict = {
        'english': 'Hello',
        'swedish': 'Hej',
        'spanish': 'Hola'
    }
    name = input("Name?")

    keys_list = greetings_dict.keys()
    languages = ''
    for key in keys_list:
        languages += '%s ' %key
    input_var = input("Languge? opntions: %s" %languages)
    language = input_var if input_var is not '' else random.choice(list(greetings_dict.keys()))

    max_iterations = random.randint(1, 10)
    current_iteration = 0
    while current_iteration < max_iterations:
        print('%s %s' % (greetings_dict.get(language), name) )
        current_iteration += 1


greetings_dict = {
    'english': 'Hello',
    'swedish': 'Hej',
    'spanish': 'Hola'
    }


def hello_method():
    name = read_input("Name?")
    keys_list = greetings_dict.keys()
    languages = ''
    for key in keys_list:
        languages += '%s ' %key
    input_var = read_input("Languge? opntions: %s" %languages)
    language = input_var if input_var is not '' else random.choice(list(greetings_dict.keys()))

    max_iterations = random.randint(1, 10)
    current_iteration = 0
    while current_iteration < max_iterations:
        print('%s %s' % (greetings_dict.get(language), name) )
        current_iteration += 1


def read_input(prompt_text):
    keyboard_input = input(prompt_text)
    if keyboard_input == 'exit':
        sys.exit()
    else:
        return keyboard_input


#---- Implement "tell me  more"
# The user can keep asking about intrests until they say exit
# ADvanced: if user says add -> They can add more options to the dictionary

#greetings_dict[key] = value
# as in greetings_dict['german'] = 'Guten tag'






